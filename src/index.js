// Ajout de Three.js et des codes complémentaires
import * as THREE from 'https://cdn.skypack.dev/three@0.136.0'
import { ColladaLoader } from 'https://cdn.skypack.dev/three@0.136.0/examples/jsm/loaders/ColladaLoader.js'
import { OrbitControls } from 'https://cdn.skypack.dev/three@0.136.0/examples/jsm/controls/OrbitControls.js';
import { WEBGL } from 'https://cdn.skypack.dev/three@0.136.0/examples/jsm/WebGL';
import { MyFirstPersonControls } from '../modules/MyFirstPersonControls.mjs';

if (!WEBGL.isWebGLAvailable()) {
	const warning = WEBGL.getWebGLErrorMessage();
	document.body.appendChild(warning);
}

var renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
renderer.setClearColor(0x87ceeb, 1.0);
renderer.clear();

var width = renderer.domElement.width;
var height = renderer.domElement.height;
var fov = 45;
var aspect = width/height;
var near = 0.1;
var far = 500;
var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);

camera.position.set(-80, 10, 0);

camera.up = new THREE.Vector3(0, 1, 0);

camera.lookAt(0, 0, 0);

var controls = new MyFirstPersonControls(camera);
controls.setLookHeight(8.0);
controls.setMoveSpeed(50.0);

const clock = new THREE.Clock();

var scene = new THREE.Scene();

var gridHelper = new THREE.GridHelper(10, 10);
scene.add(gridHelper);

var axesHelper = new THREE.AxesHelper(10);
scene.add(axesHelper);

var colladaLoader = new ColladaLoader();

// ---------- Activation des ombres ----------

renderer.shadowMap.enabled = true;
// Par défaut : ombres "dures"
//renderer.shadowMap.type = THREE.PCFShadowMap;
// Ombres douces
renderer.shadowMap.type = THREE.PCFSoftShadowMap;

// ---------- Création des sources de lumière ---------- 

// Ajout d'une lumière ambiante
let ambient = new THREE.AmbientLight(0x555555);
scene.add(ambient);
	
// Ajout d'une lumière directionnelle
let light1 = new THREE.DirectionalLight(0xffffff, 1.0);
light1.position.set(-100, 100, -100);
scene.add(light1);

// La source de lumière directionnelle produit des ombres
light1.castShadow = true;

// Propriétés de l'ombre
// Résolution de la shadow map (par défaut :
// 512*512)
light1.shadow.mapSize.width = 2048;
light1.shadow.mapSize.height = 2048;
// Propriétés de la projection depuis
// la source de lumière
let d = 200;
light1.shadow.camera.left = -d;
light1.shadow.camera.right = d;
light1.shadow.camera.top = d;
light1.shadow.camera.bottom = -d;			
light1.shadow.camera.near = 0.5;
light1.shadow.camera.far = 500;
light1.shadow.bias = 0.0005;
	
// Ajout d'une lumière ponctuelle (pour atténuer les ombres
// dans la salle sud)
let light2 = new THREE.PointLight(0xffffff, 0.5, 50);
light2.position.set(-50, 20, -25);
scene.add(light2);

// ---------- Sol ---------- 

let floorMaterial = new THREE.MeshLambertMaterial({ color: 0xaaaaaa });
let floorGeometry = new THREE.PlaneGeometry(200, 100, 200, 100);
floorGeometry.rotateX(- Math.PI / 2);
let floor = new THREE.Mesh(floorGeometry, floorMaterial);
// Le plan du sol reçoit des ombres
floor.receiveShadow = true;
var floorTexture = new THREE.TextureLoader().load(
	'assets/textures/floor.png',
);
floorTexture.wrapS = THREE.RepeatWrapping;
floorTexture.wrapT = THREE.RepeatWrapping;
floorTexture.repeat.set(10,5);
floorMaterial.map = floorTexture;
scene.add(floor);

// ---------- Murs ----------
	
// Matériau des murs
let wallMaterial = new THREE.MeshPhongMaterial({ color: 0xffffff });
	
// Géométrie des murs wall E et W
let wallGeometryX = new THREE.BoxGeometry(200, 20, 1);

let wallE = new THREE.Mesh(wallGeometryX, wallMaterial);
wallE.position.z = 50;
wallE.position.y = 10;
wallE.castShadow = true;
wallE.receiveShadow = true;
scene.add(wallE);

let wallW = new THREE.Mesh(wallGeometryX, wallMaterial);
wallW.position.z = -50;
wallW.position.y = 10;
wallW.castShadow = true;
wallW.receiveShadow = true;
scene.add(wallW);

// Géométrie des murs wall N et S
let wallGeometryZ = new THREE.BoxGeometry(1, 20, 100);

let wallN = new THREE.Mesh(wallGeometryZ, wallMaterial);
wallN.position.x = 100;
wallN.position.y = 10;
wallN.castShadow = true;
wallN.receiveShadow = true;
scene.add(wallN);

let wallS = new THREE.Mesh(wallGeometryZ, wallMaterial);
wallS.position.x = -100;
wallS.position.y = 10;
wallS.castShadow = true;
wallS.receiveShadow = true;
scene.add(wallS);

// ---------- Cloison centrale ----------

let midWallGeom = new THREE.BoxGeometry(1, 20, 40);

// Partie E
let midWallE = new THREE.Mesh(midWallGeom, wallMaterial);
midWallE.position.z = 30;
midWallE.position.y = 10;
midWallE.castShadow = true;
midWallE.receiveShadow = true;
scene.add(midWallE);

// Partie W
let midWallW = new THREE.Mesh(midWallGeom, wallMaterial);
midWallW.position.z = -30;
midWallW.position.y = 10;
midWallW.castShadow = true;
midWallW.receiveShadow = true;
scene.add(midWallW);

// ---------- Verrière ----------

// Géométrie sphérique
let sphereGeometry = new THREE.SphereGeometry(80, 20, 10, 0, Math.PI*2, 0, Math.PI/2);
sphereGeometry.scale(1.8, 0.5, 1);
sphereGeometry.translate(0, 8, 0);

// Wireframe
let edgesGeom = new THREE.EdgesGeometry(sphereGeometry); // or WireframeGeometry
let wireframeMat = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
let sphere = new THREE.LineSegments(edgesGeom, wireframeMat);
scene.add(sphere);

const box1Geom = new THREE.BoxGeometry(20, 3, 5);
const box1 = new THREE.Mesh(box1Geom);
box1.translateX(-50);
scene.add(box1);

const box2Geom = new THREE.BoxGeometry(8, 2, 8);
const box2 = new THREE.Mesh(box2Geom);
box2.translateX(50);
scene.add(box2);

function createPainting(fileName) {
	// Création d'un matériau pour le support
	let paintingMat = new THREE.MeshLambertMaterial({
		color: 0xaaaaaa
	});
	// Création d'une géométrie de type Box pour le support
	let paintingGeom = new THREE.BoxGeometry(10, 10, 0.5);
	// Création d'un matériau pour chaque face de l'objet
	// représentant le support du tableau
	let materials = [
		paintingMat,
		paintingMat,
		paintingMat,
		paintingMat,
		paintingMat,
		paintingMat
	];
	// Création de l'objet représentant le support
	let painting = new THREE.Mesh(paintingGeom, materials);
	// Les tableaux projettent et reçoivent des ombres
	painting.castShadow = true;
	painting.receiveShadow = true;
	// Ajout de la peinture à la scène
	scene.add(painting);

	// Chargement d'une image de peinture et placage sur une face
	// (face d'indice 5) de l'objet représentant le support
	new THREE.TextureLoader()
		.load(fileName,
			function(tex) {
				// Calcul du rapport largeur/hauteur du tableau
				let aspectRatio = tex.image.width/tex.image.height;
				// Mise à jour du matériau pour la face d'indice 5
				materials[5] = new THREE.MeshBasicMaterial({
					map: tex
				});
				// Mise à l'échelle en fonction du
				// rapport largeur/hauteur
				painting.scale.set(aspectRatio, 1.0, 1.0);
				// Indication que le matériau doit être mis à jour
				painting.material[5].needsUpdate = true;
				// Mise à jour du rendu
				renderer.render(scene, camera);
			});

	return painting;
}

let paint1Geom = createPainting('assets/textures/paintings/steve-johnson-1219955-unsplash.jpg');
paint1Geom.translateX(-50);
paint1Geom.translateZ(49);
paint1Geom.translateY(8);
scene.add(paint1Geom);
let paint2Geom = createPainting('assets/textures/paintings/steve-johnson-758735-unsplash.jpg');
paint2Geom.translateX(-50);
paint2Geom.translateZ(-49);
paint2Geom.translateY(8);
paint2Geom.rotateY(Math.PI);
scene.add(paint2Geom);

// Création d'un matériau
let victoireMat = new THREE.MeshLambertMaterial({
	color: 0xdddddd
});
// Chargement du modèle de la Victoire
colladaLoader.load('assets/models/victoire.dae',
	// onLoad callback
	function(collada) {
		let dae = collada.scene.children[0];
		// Application du matériau à l'objet
		dae.material = victoireMat;
		// Transformations
		dae.rotateZ(Math.PI/2);
		dae.rotateX(Math.PI);
		dae.scale.set(0.8, 0.8, 0.8);
		dae.position.set(50, 0.5, 0);
		// L'objet projette et reçoit des ombres
		dae.castShadow = true;
		dae.receiveShadow = true;
		// Ajout de l'objet à la scène
		scene.add(dae);
		console.log('Loading complete!');
		renderer.render(scene, camera);
	},
	// onProgress callback
	function (xhr) {
		console.log('Loading in progress...');
	},
	// onError callback
	function(err) {
		console.log('An error happened');
	}
);

window.addEventListener('resize', onWindowResize, false);

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.render(scene, camera);
}

function render() {
	if (camera.position.x <= -98) {
		camera.position.x = -98;
	} else if (camera.position.x >= 98) {
		camera.position.x = 98;
	} else if (camera.position.z <= -48) {
		camera.position.z = -48;
	} else if (camera.position.z >= 48) {
		camera.position.z = 48;
	} else if (camera.position.z <= -8 && camera.position.x === -2) {
		camera.position.z = -8;
	} else if (camera.position.z >= 8 && camera.position.x === 2) {
		camera.position.z = 8;
	}

	controls.update(clock.getDelta());
	renderer.render(scene, camera);
	requestAnimationFrame(render);
	renderer.render(scene, camera);
}

render();
